from setuptools import setup

setup(name='storjstat-api',
      version='0.0.1',
      description='Local storj stats via restful api',
      url='https://gitlab.com/Jedimaster0/storjstat-api',
      author='Nathan Snow',
      author_email='admin@mimir-tech.org',
      license='GPLv3',
      packages=['app'],
      install_requires=['flask','flask-restful', 'python-dotenv'],
      zip_safe=False)
