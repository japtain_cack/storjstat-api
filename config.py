import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

class Config(object):
    SERVER_NAME = os.environ.get('APP_SERVERNAME' ,'127.0.0.1:5000')
    DEBUG = os.environ.get('APP_DEBUG', False) == 'True'
    JSON_AS_ASCII = os.environ.get('APP_JSONASASCII', False) == 'True'