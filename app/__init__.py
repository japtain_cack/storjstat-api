from flask import Flask, current_app
from flask_restful import Api
from subprocess import Popen, PIPE, SubprocessError
import json, sys, shlex, os
from config import Config

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    from app.api import bp as api_bp
    app.register_blueprint(api_bp)
    
    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    return app

def storjshare_status():
    try:
        args = shlex.split(os.environ.get('STORJSHARE_PATH', '/home/storj/node_modules/.bin/storjshare') + ' status -j')
        current_app.logger.debug('args: ' + str(args))

        proc = Popen(args, stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        current_app.logger.debug('storjshare process raw output: %s', out)
        current_app.logger.debug('storjshare process error: %s', err)
        current_app.logger.debug('storjshare process exitcode: %s', exitcode)

        j = json.loads(out.decode('utf-8').strip())
        current_app.logger.debug('json raw output: %s', str(j))
        return j

    except UnicodeDecodeError as err: 
        current_app.logger.error('Unable to decode unicode data from process: %s', str(err))
        return {"error": "Unable to decode unicode data from process", "exception": str(err)}
    except json.decoder.JSONDecodeError as err:
        current_app.logger.error('Unable to decode json data: %s', str(err))
        return {"error": "Unable to decode json data", "exception": str(err)}
    except FileNotFoundError as err:
        current_app.logger.error('Storjshare path is invalid: %s', str(err))
        return {"error": "Storjshare path is invalid", "exception": str(err)}
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        err = exc_type, fname, exc_tb.tb_lineno
        current_app.logger.error('An unknown error has occurred: %s', str(err))
        return {"error": "Unexpected error", "exception": str(err)}