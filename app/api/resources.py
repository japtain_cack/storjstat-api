from flask_restful import Api, Resource
from app.api import bp
from app import storjshare_status

api = Api(bp)

class get_status(Resource):
    def get(self):
        return storjshare_status()

api.add_resource(get_status, '/status')
