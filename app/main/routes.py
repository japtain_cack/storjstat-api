from flask import render_template, current_app
from app.main import bp
from app import storjshare_status

@bp.route('/')
@bp.route('/index')
def index():
    j = storjshare_status()
    return render_template('dashboard/dashboard.html', storjNodes=j, title='StorjStat')
